package com.example.laviniabratis.recognitionapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.SpeechRecognizer;

import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

public class MainActivity extends VoiceDetectorActivity {

    private SpeechRecognizer recognizer;
    private static final String COMMAND_SEARCH = "button_commands";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setLayout();
    }

    private void setLayout(){
        Button btnOne = (Button) findViewById(R.id.btn1);
        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GetUserDataActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void reset(){
        if (recognizer != null) {
            recognizer.stop();
            recognizer.startListening(COMMAND_SEARCH, 10000);
        }
    }

    protected void setupRecognizer(File assetsDir){

        try {
            recognizer = defaultSetup()
                    .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                    .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                            // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                    .setRawLogDir(assetsDir)

                            // Threshold to tune for keyphrase to balance between false alarms and misses
                    .setKeywordThreshold((float)1)

                            // Use context-independent phonetic search, context-dependent is too slow for mobile
                    .setBoolean("-allphone_ci", true)

                    .getRecognizer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recognizer.addListener(MainActivity.this);

        // Create keyword-activation search.
        File commands = new File(assetsDir, "button_commands.gram");
        recognizer.addGrammarSearch(COMMAND_SEARCH, commands);
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis != null)
        {
            Intent intent = null;
            String text = hypothesis.getHypstr();
            if (text.equals(SELECT_ONE)){
                intent = new Intent(MainActivity.this, GetUserDataActivity.class);
                Toast.makeText(getApplicationContext(), "select one", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_TWO)) {
                Toast.makeText(getApplicationContext(), "select two", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_THREE)) {
                Toast.makeText(getApplicationContext(), "select three", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_FOUR)) {
                Toast.makeText(getApplicationContext(), "select four", Toast.LENGTH_SHORT).show();
            } else if (text.equals(DISMISS)){
                Toast.makeText(getApplicationContext(), "dismiss", Toast.LENGTH_SHORT).show();
                intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
            }

            if (intent != null){
                startActivity(intent);
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        initSpeechRecognizer();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }
}
