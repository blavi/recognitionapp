package com.example.laviniabratis.recognitionapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;

/**
 * Created by lavinia.bratis on 8/27/15.
 */
public abstract class VoiceDetectorActivity extends Activity implements RecognitionListener {

    protected static final String SELECT_ONE = "select one";
    protected static final String SELECT_TWO = "select two";
    protected static final String SELECT_THREE = "select three";
    protected static final String SELECT_FOUR = "select four";
    protected static final String DISMISS = "dismiss";
    private ProgressDialog initRecognizerPD;

    protected void initSpeechRecognizer(){
        new AsyncTask<Void, Void, Exception>() {

            @Override
            protected void onPreExecute(){
                initRecognizerPD = ProgressDialog.show(VoiceDetectorActivity.this, null, VoiceDetectorActivity.this.getResources().getString(R.string.start_recognizer));
            }

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(VoiceDetectorActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                initRecognizerPD.dismiss();
                if (result != null) {
                    Toast.makeText(VoiceDetectorActivity.this, VoiceDetectorActivity.this.getResources().getString(R.string.fail_recognizer), Toast.LENGTH_LONG).show();
                } else {
                    reset();
                }
            }
        }.execute();
    }

    protected abstract void setupRecognizer(File assetsDir);


    protected abstract void reset();

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onEndOfSpeech() {
        reset();
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis){

    }

    @Override
    public void onResult(Hypothesis hypothesis) {

    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onTimeout() {
        reset();
    }
}
