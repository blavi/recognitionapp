package com.example.laviniabratis.recognitionapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.SpeechRecognizer;

import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

/**
 * Created by lavinia.bratis on 8/26/15.
 */
public class GetUserDataActivity extends VoiceDetectorActivity {

    private SpeechRecognizer recognizer;
    private static final String COMMAND_SEARCH = "user_commands";
    private static final String SELECT_NAME = "select name";
    private static final String SELECT_EMAIL = "select email";
    private static final String SELECT_SHOW = "select show";
    private static final String BACK = "back";

    private Spinner dropdown;
    private EditText name, email;
    private LinearLayout data;
    private ListView list;
    private Button show;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setLayout();
    }

    private void setLayout(){
        setContentView(R.layout.user_data);

        list = (ListView)findViewById(R.id.list);
        String[] items = new String[]{"one", "two", "three", "four"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        list.setAdapter(adapter);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);

        show = (Button) findViewById(R.id.getList);
    }

    public void show(View v){
        list.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setupRecognizer(File assetsDir) {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        try {
            recognizer = defaultSetup()
                    .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                    .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                            // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                    .setRawLogDir(assetsDir)

                            // Threshold to tune for keyphrase to balance between false alarms and misses
                    .setKeywordThreshold((float)1)

                            // Use context-independent phonetic search, context-dependent is too slow for mobile
                    .setBoolean("-allphone_ci", true)

                    .getRecognizer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recognizer.addListener(GetUserDataActivity.this);

        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        File commands = new File(assetsDir, "user_commands.gram");
        recognizer.addGrammarSearch(COMMAND_SEARCH, commands);
    }

    @Override
    protected void reset() {
        if (recognizer != null) {
            recognizer.stop();
            recognizer.startListening(COMMAND_SEARCH, 10000);
        }
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        Intent intent = null;

        if (hypothesis != null) {
            String text = hypothesis.getHypstr();

           if (text.equals(SELECT_EMAIL)){
                email.requestFocus();
                Toast.makeText(getApplicationContext(), "select email", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_NAME)) {
                name.requestFocus();
                Toast.makeText(getApplicationContext(), "select name", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_SHOW)) {
                show.performClick();
                Toast.makeText(getApplicationContext(), "select show", Toast.LENGTH_SHORT).show();
            } else if (text.equals(SELECT_ONE)) {
                list.setSelection(0);
                resetColors(0);
            } else if (text.equals(SELECT_TWO)) {
                resetColors(1);
                list.setSelection(1);
            } else if (text.equals(SELECT_THREE)) {
                resetColors(2);
                list.setSelection(2);
            } else if (text.equals(SELECT_FOUR)) {
                resetColors(3);
                list.setSelection(3);
            } else if (text.equals(BACK)){
               Toast.makeText(getApplicationContext(), "back", Toast.LENGTH_SHORT).show();
               intent = new Intent(GetUserDataActivity.this, MainActivity.class);
            } else if (text.equals(DISMISS)){
               Toast.makeText(getApplicationContext(), "dismiss", Toast.LENGTH_SHORT).show();
               intent = new Intent(Intent.ACTION_MAIN);
               intent.addCategory(Intent.CATEGORY_HOME);
            }

            if (intent != null){
                startActivity(intent);
            } else {
                reset();
            }
        }
    }

    private void resetColors(int position){
        for (int i = 0; i < list.getChildCount(); i++) {
            if (i != position){
                list.getChildAt(i).setBackgroundColor(GetUserDataActivity.this.getResources().getColor(android.R.color.transparent));
            } else {
                list.getChildAt(i).setBackgroundColor(GetUserDataActivity.this.getResources().getColor(R.color.red));
            }
        }

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                list.setVisibility(View.INVISIBLE);
            }
        }.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        initSpeechRecognizer();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }
}
